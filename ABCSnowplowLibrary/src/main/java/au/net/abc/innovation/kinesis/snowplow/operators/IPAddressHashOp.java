/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow.operators;

import au.net.abc.innovation.kinesis.snowplow.SnowplowEventModel;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Simple operator that hashes the IP address
 * and replaces it with its hex string representation
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */

public class IPAddressHashOp implements ISnowplowEventOperator{
    
    private static final Log LOG = LogFactory.getLog(IPAddressHashOp.class);
    
    @Override
    public SnowplowEventModel apply(SnowplowEventModel event){
        try{
            String ipAddress = DigestUtils.md5Hex(event.getUser_ipaddress());
            event.setUser_ipaddress(ipAddress);
        }
        catch(Exception e){
            LOG.error("Exception applying operator to user IP address " + event.getUser_ipaddress(), e);
        }
        return event;
    }
}
