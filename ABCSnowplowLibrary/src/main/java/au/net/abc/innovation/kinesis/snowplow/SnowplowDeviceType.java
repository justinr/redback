/**
 * Copyright 2015 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.kinesis.snowplow;

/**
 * Enumeration representing device type values used in Snowplow
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public enum SnowplowDeviceType {
    
    Mobile,
    Computer,
    Tablet,
    Game_Console,
    Digital_Media_Receiver;
    
    @Override
    public String toString(){
        String val = null;
        switch(this){
            case Mobile:{                   val= "Mobile"; break;}
            case Computer:{                 val= "Computer"; break;}
            case Tablet:{                   val= "Tablet"; break;}
            case Game_Console:{             val= "Game Console"; break;}
            case Digital_Media_Receiver:{   val= "Digital Media Receiver"; break;}
        }
        return val;
    }
}