/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

package au.net.abc.innovation;

import au.net.abc.innovation.kinesis.snowplow.SnowplowKinesisHandlerFactory;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Simple extension to support some extra configuration options
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public class RedbackConfiguration extends KinesisConnectorConfiguration{
    
    public static final String PROP_REDSHIFT_COPY_OPTIONS   = "redshiftCopyOptions";
    
    public static final String PROP_SNOWPLOW_STREAM_SCHEMA  = "snowplowStreamSchemaVersion";
    
    public static final String PROP_EVENT_LOGS_APPS         = "event.logging.appids";
    
    public static final String PROP_EVENT_FILTER_APPS       = "event.filters.appids";
    public static final String PROP_EVENT_OPERATORS         = "event.operators";
    
    private List<String> redshiftCopyOptions;
    private final String snowplowStreamSchemaVersion;
    
    private List<String> eventAppLogList;
    private List<String> eventFiltersAppIdExclusionList;
    private List<String> eventOperatorClassNames;
    
    public RedbackConfiguration(Properties properties, AWSCredentialsProvider credentialsProvider) {
        super(properties, credentialsProvider);
        
        String rsCopyOptions = properties.getProperty(PROP_REDSHIFT_COPY_OPTIONS, null);
        if(rsCopyOptions != null){
            redshiftCopyOptions = new ArrayList<String>();
            String[] options = rsCopyOptions.trim().split(",");
            for(String option : options){
                redshiftCopyOptions.add(option.trim());
            }
        }
        snowplowStreamSchemaVersion = properties.getProperty(PROP_SNOWPLOW_STREAM_SCHEMA, 
                                                            SnowplowKinesisHandlerFactory.CURRENT_VERSION);
        
        String eventAppLogs = properties.getProperty(PROP_EVENT_LOGS_APPS, null);
        if(eventAppLogs != null){
            eventAppLogList = new ArrayList<String>();
            String[] appIds = eventAppLogs.trim().split(",");
            for(String appId : appIds){
                eventAppLogList.add(appId.trim());
            }
        }
        
        String eventFiltersAppIds = properties.getProperty(PROP_EVENT_FILTER_APPS, null);
        if(eventFiltersAppIds != null){
            eventFiltersAppIdExclusionList = new ArrayList<String>();
            String[] appIds = eventFiltersAppIds.trim().split(",");
            for(String appId : appIds){
                eventFiltersAppIdExclusionList.add(appId.trim());
            }
        }
        
        String eventOperators = properties.getProperty(PROP_EVENT_OPERATORS, null);
        if(eventOperators != null){
            eventOperatorClassNames = new ArrayList<String>();
            String[] operatorClassNames = eventOperators.trim().split(",");
            for(String operatorClassName : operatorClassNames){
                eventOperatorClassNames.add(operatorClassName.trim());
            }
        }
    }
    
    public boolean hasRedshiftOptions(){
        return redshiftCopyOptions != null && redshiftCopyOptions.size() > 0;
    }
    
    public List<String> getRedshiftOptions(){
        return redshiftCopyOptions;
    }

    public String getSnowplowStreamSchemaVersion() {
        return snowplowStreamSchemaVersion;
    }
    
    public boolean hasEventApplicationLogs(){
        return eventAppLogList != null && eventAppLogList.size() > 0;
    }
    
    public List<String> getEventApplicationLogs(){
        return eventAppLogList;
    }
    
    public boolean hasApplicationFilters(){
        return eventFiltersAppIdExclusionList != null && eventFiltersAppIdExclusionList.size() > 0;
    }
    
    public List<String> getApplicationFilters(){
        return eventFiltersAppIdExclusionList;
    }
    
    public boolean hasOperators(){
        return eventOperatorClassNames != null && eventOperatorClassNames.size() > 0;
    }
    
    public List<String> getOperators(){
        return eventOperatorClassNames;
    }
}