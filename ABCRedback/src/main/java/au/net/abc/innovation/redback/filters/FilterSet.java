/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation.redback.filters;

import au.net.abc.innovation.RedbackConfiguration;
import au.net.abc.innovation.kinesis.snowplow.SnowplowEventModel;
import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import com.amazonaws.services.kinesis.connectors.interfaces.IFilter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sam
 */

public class FilterSet implements IFilter<SnowplowEventModel>{
    
    private List<IFilter<SnowplowEventModel>> filters;
    
    public FilterSet(KinesisConnectorConfiguration configuration){
        if(configuration instanceof RedbackConfiguration){
            RedbackConfiguration redbackConfiguration = (RedbackConfiguration)configuration;
            if(redbackConfiguration.hasApplicationFilters()){
                filters = new ArrayList<IFilter<SnowplowEventModel>>();
                filters.add(new ApplicationFilter(redbackConfiguration.getApplicationFilters()));
            }                
        }
    }
        
    @Override
    public boolean keepRecord(SnowplowEventModel record){
        if(filters != null){
            for(IFilter filter : filters){
                if(!filter.keepRecord(record)){
                    return false;
                }
            }
        }
        return true;
    }    
}
