/**
 * Copyright 2014 Australian Broadcasting Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */
package au.net.abc.innovation;

import au.net.abc.innovation.kinesis.snowplow.SnowplowEventModel;
import au.net.abc.innovation.redback.filters.FilterSet;
import com.amazonaws.services.kinesis.connectors.KinesisConnectorConfiguration;
import com.amazonaws.services.kinesis.connectors.impl.AllPassFilter;
import com.amazonaws.services.kinesis.connectors.impl.BasicMemoryBuffer;
import com.amazonaws.services.kinesis.connectors.interfaces.IBuffer;
import com.amazonaws.services.kinesis.connectors.interfaces.IEmitter;
import com.amazonaws.services.kinesis.connectors.interfaces.IFilter;
import com.amazonaws.services.kinesis.connectors.interfaces.IKinesisConnectorPipeline;
import com.amazonaws.services.kinesis.connectors.interfaces.ITransformer;

/**
 * Simple application pipeline.
 * 
 * @author Sam Mason (sam.mason@abc.net.au)
 */
public class RedbackBasicPipeline implements IKinesisConnectorPipeline<SnowplowEventModel, byte[]> {

    @Override
    public IEmitter<byte[]> getEmitter(KinesisConnectorConfiguration configuration) {
        return new RedbackEmitter(configuration);
    }

    @Override
    public IBuffer<SnowplowEventModel> getBuffer(KinesisConnectorConfiguration configuration) {
        return new BasicMemoryBuffer<SnowplowEventModel>(configuration);
    }

    @Override
    public ITransformer<SnowplowEventModel, byte[]> getTransformer(KinesisConnectorConfiguration configuration) {
        return new RedbackModelTransformer(configuration);
    }

    @Override
    public IFilter<SnowplowEventModel> getFilter(KinesisConnectorConfiguration configuration) {
        return new FilterSet(configuration);
    }

}